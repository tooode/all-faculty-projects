 .386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
extern malloc: proc
extern memset: proc

includelib canvas.lib
extern BeginDrawing: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
;aici declaram date
window_title DB "X&0",0
area_width EQU 300;tabla de joc
area_height EQU 300;tabla de joc
area DD 0
counter DD 0

XX0 DD 80
Y0 DD 90

square_00 dd 0
square_01 dd 4
square_02 dd 8
square_10 dd 12
square_11 dd 16
square_12 dd 20
square_20 dd 24
square_21 dd 28
square_22 dd 32
val_matrice dd 0
zeroo DD 0

matrice DD 9 dup(0)
X DD 0
Y DD 0

check_out DD 0
const_zero EQU 0
nr_miscari DD 9
nr_miscari_min DD 5
verifX0 DD 0

;vertic DD 150;variabila utilizata in incercarea de a trasa liniile cu ajutorul unei structuri repetitive
arg1 EQU 8
arg2 EQU 12
arg3 EQU 16
arg4 EQU 20
symbol_width_X0 EQU 40
symbol_height_X0 EQU 39
symbol_width EQU 10
symbol_height EQU 20
include digits.inc
include letters.inc
include X0.inc

.code
; procedura make_text afiseaza o litera sau o cifra la coordonatele date
; arg1 - simbolul de afisat (litera sau cifra)
; arg2 - pointer la vectorul de pixeli
; arg3 - pos_x
; arg4 - pos_y
linie_verticala MACRO i, j
LOCAL repeta
	mov esi, i ;contor i
	mov edi, j ;contor j
	mov ecx, 120 ;de cate ori sa mi se repete bucla de afisare, adica cat de lunga este linia
	mov ebx, area 	
	repeta:
	;area este zona de memorie in care am salvat adresa, am nevoie de valoarea de la area 
	mov eax, area_width
	mul esi ;eax=i*area_width
	add eax, edi
	mov dword ptr[ebx + eax*4], 000h
	inc edi
	;inc esi
	loop repeta
endm
linie_verticala_BUTON MACRO i, j
LOCAL repeta
	mov esi, i ;contor i
	mov edi, j ;contor j
	mov ecx, 74 ;de cate ori sa mi se repete bucla de afisare, adica cat de lunga este linia
	mov ebx, area 	
	repeta:
	;area este zona de memorie in care am salvat adresa, am nevoie de valoarea de la area 
	mov eax, area_width
	mul esi ;eax=i*area_width
	add eax, edi
	mov dword ptr[ebx + eax*4], 000h
	inc edi
	;inc esi
	loop repeta
endm
linie_orizontala MACRO i, j
LOCAL repeta
	mov esi, i ;contor i
	mov edi, j ;contor j
	mov ecx, 120 ;de cate ori sa mi se repete bucla de afisare, adica cat de lunga este linia
	mov ebx, area 	
	repeta:
	;area este zona de memorie in care am salvat adresa, am nevoie de valoarea de la area  
	mov eax, area_width
	mul esi ;eax=i*area_width
	add eax, edi
	mov dword ptr[ebx + eax*4], 000h
	;inc edi
	inc esi
	loop repeta
endm
linie_orizontala_BUTON MACRO i, j
LOCAL repeta
	mov esi, i ;contor i
	mov edi, j ;contor j
	mov ecx, 20 ;de cate ori sa mi se repete bucla de afisare, adica cat de lunga este linia
	mov ebx, area 	
	repeta:
	;area este zona de memorie in care am salvat adresa, am nevoie de valoarea de la area  
	mov eax, area_width
	mul esi ;eax=i*area_width
	add eax, edi
	mov dword ptr[ebx + eax*4], 000h
	;inc edi
	inc esi
	loop repeta
endm



make_text proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax, 'A'
	jl make_digit
	cmp eax, 'Z'
	jg make_digit
	sub eax, 'A'
	lea esi, letters
	jmp draw_text
make_digit:
	cmp eax, '0'
	jl make_space
	cmp eax, '9'
	jg make_space
	sub eax, '0'
	lea esi, digits
	jmp draw_text
make_space:
	mov eax, 26 ; de la 0 pana la 25 sunt litere, 26 e space
	lea esi, letters
	jmp draw_text
draw_text:
	mov ebx, symbol_width
	mul ebx
	mov ebx, symbol_height
	mul ebx
	add esi, eax
	mov ecx, symbol_height
bucla_simbol_linii:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_width
bucla_simbol_coloane:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb
	mov dword ptr [edi], 0
	jmp simbol_pixel_next
simbol_pixel_alb:
	mov dword ptr [edi], 0FFFFFFh
simbol_pixel_next:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane
	pop ecx
	loop bucla_simbol_linii
	popa
	mov esp, ebp
	pop ebp
	ret
make_text endp

; un macro ca sa apelam mai usor desenarea simbolului
make_text_macro macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call make_text
	add esp, 16
endm

make_x0 proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax,'X'
	je make_x
	cmp eax,'0'
	je make_0
make_x:
	mov eax, 0
	lea esi, X0
	jmp draw_text_x0 
make_0:
	mov eax, 1
	lea esi, X0
	jmp draw_text_x0
draw_text_x0:
	mov ebx, symbol_height_X0
	mul ebx
	mov ebx, symbol_height_X0
	mul ebx
	add esi, eax
	mov ecx, symbol_height_X0
bucla_simbol_linii:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height_X0
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_height_X0
bucla_simbol_coloane:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb
	mov dword ptr [edi], 0
	jmp simbol_pixel_next
simbol_pixel_alb:
	mov dword ptr [edi], 0FFFFFFh
simbol_pixel_next:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane
	pop ecx
	loop bucla_simbol_linii
	popa
	mov esp, ebp
	pop ebp
	ret
make_x0 endp

make_x0_macro macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call make_x0
	add esp, 16
endm
in_table proc  
	mov esi, edx
	cmp ebx, XX0
	jl return_false
	mov eax, 3
	mov ecx, symbol_width_X0
	mul ecx
	add eax, XX0
	cmp ebx, eax
	jge return_false
	mov edx, esi
	cmp edx, Y0
	jl return_false
	mov eax, 3
	mov ecx, symbol_width_X0
	mul ecx
	add eax, Y0
	mov edx, esi
	cmp edx, eax
	jge return_false
	mov eax, 1
	mov check_out, eax        
	jmp outer2
return_false:
	mov eax, 0
	mov check_out, eax
outer2:
	ret
in_table endp

make_it_matrix proc
	push ebp
	mov ebp, esp
	push ebx
	push ecx
	mov eax, [ebp+arg1]
	mov ebx, 4
	mul ebx
	mov esi, eax			
	mov eax, [ebp+arg2]
	mul ebx
	mov ebx, eax
	mov eax, 3				
	mul ebx
	mov ebx, eax
	mov eax, matrice[ebx+esi]	
	cmp eax, 0 
	jne iesire_transpunere_in_matrice
	mov ecx, [ebp+arg3] 
	cmp ecx, 0 
	jne este_X
este_0:
	mov eax, 2
	mov matrice[ebx+esi], eax
	mov eax, 0
	jmp aici
este_X:
	mov eax, 1
	mov matrice[ebx+esi], eax
	mov eax, 0
	jmp aici
iesire_transpunere_in_matrice:
	mov edx, 1				
	sub edx, verifX0
	mov verifX0, edx
aici:
	pop ecx
	pop ebx
	mov esp, ebp
	pop ebp
	ret
make_it_matrix endp	
check_winner_line proc
	mov esi, matrice[ebx]
again:
	cmp dword ptr [esi+square_00], 1
	jne is_0
	cmp dword ptr [esi+square_10], 1
	jne a_doua_linie0
	cmp dword ptr [esi+square_20], 1
	jne a_treia_linie0
is_x:
	cmp dword ptr [esi+square_00], 1
	jne a_doua_linie
	cmp dword ptr [esi + square_01], 1
	jne a_doua_linie
	cmp dword ptr [esi + square_02], 1
	mov val_matrice, 1
	je win
a_doua_linie:
	cmp dword ptr [esi + square_10], 1
	jne a_treia_linie
	cmp dword ptr [esi + square_11], 1
	jne a_treia_linie
	cmp dword ptr [esi + square_12], 1
	mov val_matrice, 1
	je win
a_treia_linie:
	cmp dword ptr [esi + square_20], 1
	jne final
	cmp dword ptr [esi + square_21], 1
	jne final
	cmp dword ptr [esi + square_22], 1
	mov val_matrice, 1
    je win
is_0:
	cmp dword ptr [esi+square_00], 2
	jne a_doua_linie0
	cmp dword ptr [esi + square_01], 2
	jne a_doua_linie0
	cmp dword ptr [esi + square_02], 2
	mov val_matrice, 2
	je win
a_doua_linie0:
	cmp dword ptr [esi + square_10], 2
	jne a_treia_linie0
	cmp dword ptr [esi + square_11], 2
	jne a_treia_linie0
	cmp dword ptr [esi + square_12], 2
	mov val_matrice, 2
	je win
a_treia_linie0:
	cmp dword ptr [esi + square_20], 2
	jne final
	cmp dword ptr [esi + square_21], 2
	jne final
	cmp dword ptr [esi + square_22], 2
	mov val_matrice, 2
    je win
win:
	cmp val_matrice,1
	je X_wins
	cmp val_matrice,2
	je O_wins
X_wins:
	mov eax,1
	jmp gata;
O_wins:
	mov eax,2
	jmp gata
final:
	 mov eax, 0
	 jmp gata;
gata:
	 ret
check_winner_line endp
check_winner_column proc
mov esi, matrice[ebx]
	cmp dword ptr [esi+square_00], 1
	jne is_0
	cmp dword ptr [esi+square_01], 1
	jne a_doua_coloana0
	cmp dword ptr [esi+square_02], 1
	jne a_treia_coloana0
is_x:
	cmp dword ptr [esi+square_00], 1
	jne a_doua_coloana
	cmp dword ptr [esi + square_10], 1
	jne a_doua_coloana
	cmp dword ptr [esi + square_20], 1
	mov val_matrice, 1
	je win2
a_doua_coloana:
	cmp dword ptr [esi + square_01], 1
	jne a_treia_coloana
	cmp dword ptr [esi + square_11], 1
	jne a_treia_coloana
	cmp dword ptr [esi + square_21], 1
	mov val_matrice, 1
	je win2
a_treia_coloana:
	cmp dword ptr [esi + square_02], 1
	jne final2
	cmp dword ptr [esi + square_12], 1
	jne final2
	cmp dword ptr [esi + square_22], 1
	mov val_matrice, 1
    je win2
is_0:
	cmp dword ptr [esi+square_00], 2
	jne a_doua_coloana0
	cmp dword ptr [esi + square_10], 2
	jne a_doua_coloana0
	cmp dword ptr [esi + square_20], 2
	mov val_matrice, 2
	je win2
a_doua_coloana0:
	cmp dword ptr [esi + square_01], 2
	jne a_treia_coloana0
	cmp dword ptr [esi + square_11], 2
	jne a_treia_coloana0
	cmp dword ptr [esi + square_21], 2
	mov val_matrice, 2
	je win2
a_treia_coloana0:
	cmp dword ptr [esi + square_02], 2
	jne final2
	cmp dword ptr [esi + square_12], 2
	jne final2
	cmp dword ptr [esi + square_22], 2
	mov val_matrice, 2
    je win2
win2:
	cmp val_matrice,1
	je X_wins2
	cmp val_matrice,2
	je O_wins2
X_wins2:
	mov eax,1
	jmp gata2;
O_wins2:
	mov eax,2
	jmp gata2
final2:
	 mov eax, 0
	 jmp gata2;
gata2:
	 ret
check_winner_column endp;
check_winner_diagonala1 proc
mov esi, matrice[ebx]
	cmp dword ptr [esi+square_00], 1
	jne is_0
is_X:
	cmp dword ptr [esi+square_11], 1
	jne final3
	cmp dword ptr [esi+square_22], 1
	mov val_matrice,1
	je win3
is_0:
	cmp dword ptr [esi+square_11], 2
	jne final3
	cmp dword ptr [esi+square_22], 2
	mov val_matrice,2
	je win3
win3:
	cmp val_matrice,1
	jmp X_wins3
	cmp val_matrice,2
	jmp O_wins3
X_wins3:
	mov eax,1
	jmp gata3
O_wins3:
	mov eax,2
	jmp gata3
final3:
	mov eax,0
	jmp gata3
gata3:
	ret
check_winner_diagonala1 endp;
check_winner_diagonala2 proc
mov esi, matrice[ebx]
	cmp dword ptr [esi+square_02], 1
	jne is_0
is_X:
	cmp dword ptr [esi+square_11], 1
	jne final4
	cmp dword ptr [esi+square_20], 1
	mov val_matrice,1
	je win4
is_0:
	cmp dword ptr [esi+square_11], 2
	jne final4
	cmp dword ptr [esi+square_20], 2
	mov val_matrice,2
	je win4
win4:
	cmp val_matrice,1
	jmp X_wins4
	cmp val_matrice,2
	jmp O_wins4
X_wins4:
	mov eax,1
	jmp gata4
O_wins4:
	mov eax,2
	jmp gata4
final4:
	mov eax,0
	jmp gata4
gata4:
	ret
check_winner_diagonala2 endp;
check_winner proc
	call check_winner_line
	cmp eax,zeroo
	jne fin
	call check_winner_column
	cmp eax,zeroo
	jne fin
	call check_winner_diagonala1
	cmp eax,zeroo
	jne fin
	call check_winner_diagonala2
	cmp eax,zeroo
fin:
	ret
check_winner endp
mainX0 proc
	push ecx
	mov ebx,[ebp+arg2]
	mov edx,[ebp+arg3]
	push edx
	push ebx
	call in_table
	add esp, 8
	cmp eax, 0
	je finish_main
	mov edx, 1
	sub edx, verifX0
	mov verifX0, edx
	;aflam X
	xor edx, edx
	mov ecx, symbol_width_X0
	mov eax, [ebp+arg2]
	sub eax, XX0
	div ecx
	mov X, eax
	mov ecx, symbol_width_X0
	mul ecx
	mov ebx, eax
	add ebx, XX0
	add ebx, 1
	;aflam Y
	xor edx, edx
	mov eax, [ebp+arg3]
	sub eax, Y0
	div ecx
	mov Y, eax
	mov ecx, symbol_width_X0
	mul ecx
	add eax, Y0
	add eax, 1
	
	mov ecx, eax
	push verifX0
	push Y
	push X
	call make_it_matrix
	add esp, 12
	cmp eax, 0
	jne finish_main
	cmp verifX0, 1
	jne draw_0
	make_x0_macro 'X', area, ebx, ecx
	jmp finish_main
draw_0:
	make_x0_macro '0',area, ebx, ecx
finish_main:
	pop ecx
	ret
mainX0 endp
restart proc
	cmp ebx, 5
	jl in_afara_butonului
	cmp ebx,74
	jg in_afara_butonului
	cmp edx, 5
	jl in_afara_butonului
	cmp edx, 21
	jg in_afara_butonului
	pace_matrix:
	mov ecx, 9
	mov edi, 0
	bucla:
		mov esi, ecx
		dec esi
		mov matrice[esi*4], edi;reset matrix
	loop bucla;
	mov ebx, 0
	mov verifX0, ebx
	mov ebx, 9
	mov nr_miscari, ebx; reset moves counter
	jmp final
in_afara_butonului:
	mov eax,999
final:
	ret
restart endp
; functia de desenare - se apeleaza la fiecare click si la initializare 
; sau la fiecare interval de 200ms in care nu s-a dat click
; arg1 - evt (0 - initializare, 1 - click, 2 - s-a scurs intervalul fara click)
; arg2 - x
; arg3 - y
draw proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1]
	cmp eax, 1
	jz evt_click
	cmp eax, 2
	jz afisare_litere ; nu s-a efectuat click pe nimic
	;mai jos e codul care intializeaza fereastra cu pixeli albi
clear:
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	push 255
	push area
	call memset
	add esp, 12
	;TABLA DE JOC

 linie_verticala 90, 80
 linie_verticala 130, 80
 linie_verticala 170, 80
 linie_verticala 210,80
 
 
 linie_orizontala 90, 80
 linie_orizontala 90, 120
 linie_orizontala 90, 160
 linie_orizontala 90,200
	jmp afisare_litere
	
evt_click:
    mov ebx,[ebp+arg2]
	mov edx,[ebp+arg3]	
	push edx
	push ebx
	add esp, 8
	mov eax, 99
	cmp eax, 99
	jne clear
	
	buclaMARE:
		call mainX0 ;game procces
	mov ecx, 0
	jecxz label_reset 
		cmp eax, 0
		jne keep_counter
		cmp check_out, const_zero	;daca click s-a efectuat afara din tabela, la fel pastram
		je keep_counter
		dec nr_miscari
		jmp castig
	keep_counter:
		mov ecx, nr_miscari
	castig:
	 call check_winner_line
	 cmp eax, 1
	 je X_wins
	 cmp eax,2
	 je O_wins
	 cmp nr_miscari, 0
	 je DRAWW
	iesi:	
		jmp afisare_litere
	label_reset:
		mov ebx,[ebp+arg2]
		mov edx,[ebp+arg3]
		push edx
		push ebx
		call restart ;reset table
		add esp, 8
		cmp eax, 999
		jne clear
		jmp afisare_litere
		
	X_wins:
		mov nr_miscari, 0		
		make_text_macro 'X', area,105 , 2
		make_text_macro 'W', area,115 , 2
		make_text_macro 'I', area,125 , 2
		make_text_macro 'N', area,135 , 2
		jmp afisare_litere
	O_wins:
		mov nr_miscari,0
		make_text_macro 'O', area,105 , 2 
		make_text_macro 'W', area,115 , 2
		make_text_macro 'I', area,125 , 2
		make_text_macro 'N', area,135 , 2
		jmp afisare_litere
	DRAWW:
		make_text_macro 'D', area,105 , 2 
		make_text_macro 'R', area,115 , 2
		make_text_macro 'A', area,125 , 2
		make_text_macro 'W', area,135 , 2
		jmp afisare_litere
afisare_litere:
	 ;scriem un mesaj
	make_text_macro 'R', area, 5, 2
	make_text_macro 'E', area, 15, 2
	make_text_macro 'S', area, 25, 2
	make_text_macro 'T', area, 35, 2
	make_text_macro 'A', area, 45, 2
	make_text_macro 'R', area, 55, 2
	make_text_macro 'T', area, 65, 2
final_draw:
;CADRU BUTON

;LINIE BUTON SUS
 linie_verticala_BUTON 2,3
 linie_verticala_BUTON 3,3
;LINIE BUTON JOS
 linie_verticala_BUTON 22,3
 linie_verticala_BUTON 23,3
;LINIE BUTON STANGA
 linie_orizontala_BUTON 2,3
 linie_orizontala_BUTON 2,4 
;LINIE BUTON DREAPTA
 linie_orizontala_BUTON 2,75
 linie_orizontala_BUTON 2,76
 
 

 ;;;Incercare de desenare in bucla nereusita...
 ;mov ECX,4
;verticale:
 ;linie_verticala vertic, 100
 ;add vertic,40
;loop verticale





	popa ;reface toti registrii pe stiva, pusha ii pune pe toti
	mov esp, ebp
	pop ebp
	ret
draw endp

start:
	;alocam memorie pentru zona de desenat
	mov eax, area_width
	mov ebx, area_height 
	mul ebx ;rez in edx:eax
	shl eax, 2 ;inmultire cu 2^2 (nr poz shiftate) =4 de ce 
	push eax 
	call malloc ;returneaza ptr spre zona de memorie alocata si o pune in eax 
	add esp, 4 ;curata stiva
	mov area, eax ;in area este adresa zonei de memorie alocata cu malloc 
	;apelam functia de desenare a ferestrei
	; typedef void (*DrawFunc)(int evt, int x, int y);
	; void __cdecl BeginDrawing(const char *title, int width, int height, unsigned int *area, DrawFunc draw);
	push offset draw 
	push area 
	push area_height
	push area_width
	push offset window_title ;ptr string 
	call BeginDrawing ;asta deseneaza fereastra
	add esp, 20
	
	;terminarea programului
	push 0
	call exit
end start
