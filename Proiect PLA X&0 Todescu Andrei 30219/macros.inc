
square_macro macro x, y, square_size, color, pixels
	push pixels
	push color
	push square_size
	push y
	push x
	call square
	add ESP, 20
endm

init_screen_macro macro x, y, pixels, color
	push color
	push pixels
	push y
	push x
	call init_screen
	add esp, 16
endm

printf_nbr macro n
	pushad
	push n
	push offset format
	call printf
	add esp, 8
	popad
endm

is_in_simulation_screen_macro macro x, y
	push y
	push x
	call is_in_simulation_screen
	add esp, 8
endm

count_neighbours_macro macro x, y, cells
	push cells
	push y
	push x
	call count_neighbours
	add esp, 12
endm

draw_letter_macro macro x, y, pixels, letter
	push letter
	push pixels
	push y
	push x
	call draw_letter
	add esp, 16
endm

