
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name lcd_control -dir "D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/lcd_control/planAhead_run_1" -part xc3s500efg320-4
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/lcd_control/lcd.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/lcd_control} }
set_property target_constrs_file "char.ucf" [current_fileset -constrset]
add_files [list {char.ucf}] -fileset [get_property constrset [current_run]]
link_design
