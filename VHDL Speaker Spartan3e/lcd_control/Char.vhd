library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;

use IEEE.STD_LOGIC_UNSIGNED.ALL;

 

entity lcd is

port (clk    : in   STD_LOGIC;
           SF_D   : out  STD_LOGIC_VECTOR (3 downto 0);
           SF_CE0 : out  STD_LOGIC;
           LCD_E  : out  STD_LOGIC;
           LCD_RS : out  STD_LOGIC;
           LCD_RW : out  STD_LOGIC );

end lcd;

 

architecture Behavioral of lcd is
component lcd_ctrl is
    Port ( clk    : in   STD_LOGIC;
           rst    : in   STD_LOGIC;
           lcd    : in   STD_LOGIC_VECTOR (63 downto 0);
           SF_D   : out  STD_LOGIC_VECTOR (3 downto 0);
			  SF_CE0 : out  STD_LOGIC;
           LCD_E  : out  STD_LOGIC;
           LCD_RS : out  STD_LOGIC;
           LCD_RW : out  STD_LOGIC);
end component lcd_ctrl;
--constant datas : arr :=    (X"38",X"0c",X"06",X"01",X"C0",X"54",X"4F",X"44",X"45",X"53",X"43",X"55",X"20",X"41",X"4E",X"44",X"52",X"45",X"49",X"20",X"20",X"20"); --command and data to display                                             
function hex2ascii (hex : std_logic_vector) return std_logic_vector is
	variable ascii : std_logic_vector (7 downto 0);
begin
	if (hex > x"9") then
		ascii := x"0" & hex + x"37";
	else
		ascii := x"0" & hex + x"30";
	end if;
	return ascii;
end function hex2ascii;

signal lcd   : std_logic_vector (63 downto 0);
signal ch1   : std_logic_vector (7 downto 0);
signal ch2   : std_logic_vector (7 downto 0);
signal ch3   : std_logic_vector (7 downto 0);
signal ch4   : std_logic_vector (7 downto 0);
signal ch5   : std_logic_vector (7 downto 0);
signal ch6   : std_logic_vector (7 downto 0);
signal ch7   : std_logic_vector (7 downto 0);
signal ch8   : std_logic_vector (7 downto 0);
 

begin
lcd_ctrl_i: lcd_ctrl port map (clk => clk,
											 rst => '0',
											 lcd => lcd,
											 SF_D => SF_D,
											 SF_CE0 => SF_CE0,
											 LCD_E => LCD_E,
											 LCD_RS => LCD_RS,
											 LCD_RW => LCD_RW);
process(clk)
variable i:integer:=0;
begin
if clk'event and clk='1' then
if i<50000000 then
i:=i+1;
lcd <= X"2020202020202020";
elsif i>=50000000 and i<100000000 then
i:=i+1;


	ch1 <= X"54";--T
	ch2 <= X"4F";--O
	ch3 <= X"44";--D
	ch4 <= X"45";--E
	ch5 <= X"53";--S
	ch6 <= X"43";--C
	ch7 <= X"55";--U
	ch8 <= X"20";--' '
	lcd <= ch1 & ch2 & ch3 & ch4 & ch5 & ch6 & ch7 & ch8;
else 
i:=0;
end if;
end if;
end process;
end Behavioral;

