
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name clock -dir "D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/clock/clock/planAhead_run_4" -part xc3s500efg320-4
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/clock/clock/clock.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/clock/clock} }
set_property target_constrs_file "clock.ucf" [current_fileset -constrset]
add_files [list {clock.ucf}] -fileset [get_property constrset [current_run]]
link_design
