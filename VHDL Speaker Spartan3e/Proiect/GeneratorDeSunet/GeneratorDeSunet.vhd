library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity GeneratorDeSunet is
	Port(clk    : in   STD_LOGIC;
	sw1 : in std_logic;
	sw2 : in std_logic;
	sw3 : in std_logic;
	sw4 : in std_logic;
	SF_D   : out  STD_LOGIC_VECTOR (3 downto 0);
   SF_CE0 : out  STD_LOGIC;
   LCD_E  : out  STD_LOGIC;
   LCD_RS : out  STD_LOGIC;
   LCD_RW : out  STD_LOGIC;
	speaker : out STD_LOGIC);
end GeneratorDeSunet;

architecture Behavioral of GeneratorDeSunet is

component lcd_ctrl is
    Port ( clk    : in   STD_LOGIC;
           rst    : in   STD_LOGIC;
           lcd    : in   STD_LOGIC_VECTOR (63 downto 0);
           SF_D   : out  STD_LOGIC_VECTOR (3 downto 0);
			  SF_CE0 : out  STD_LOGIC;
           LCD_E  : out  STD_LOGIC;
           LCD_RS : out  STD_LOGIC;
           LCD_RW : out  STD_LOGIC);
end component lcd_ctrl;

component PWM is
	Port(clk: in std_logic;
	s: in std_logic_vector(15 downto 0);
	speaker: out std_logic);
end component PWM;

type mat is array (0 to 4) of std_logic_vector(15 downto 0);--MATRICEA DE NOTE
signal notes: mat:= (x"0000",x"03E8",x"2710",x"61A8",x"C350"); -- NO TONE C3 G3 E4 B4
signal lcd   : std_logic_vector (63 downto 0);

signal s : std_logic_vector(15 downto 0);

begin

lcd_ctrl_i: lcd_ctrl port map (clk => clk,
											 rst => '0',
											 lcd => lcd,
											 SF_D => SF_D,
											 SF_CE0 => SF_CE0,
											 LCD_E => LCD_E,
											 LCD_RS => LCD_RS,
											 LCD_RW => LCD_RW);
											
pwm_i: PWM port map (clk => clk,
								s=>s,
								speaker=>speaker);
											 
	process(clk,sw1,sw2,sw3,sw4)
		begin
			if sw1='1' then
				s <= notes(1);
				lcd <= X"4333202020202020";--C3
			elsif sw2='1' then
				s <= notes(2);
				lcd <= X"4733202020202020";--G3
			elsif sw3='1' then
				s <= notes(3);
				lcd <= X"4534202020202020";--E4
			elsif sw4='1' then
				s <= notes(4);
				lcd <= X"4234202020202020";--B4
			else 
				s <= notes(0);
				lcd <= X"4E4F20544F4E4520";--NO TONE
			end if;
	end process;

end Behavioral;

