library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity PWM is
	Port(clk: in std_logic;
	s: in std_logic_vector(15 downto 0);
	speaker: out std_logic);
end PWM;

architecture Behavioral of PWM is
begin
	process(clk)
		variable pwm_acc: integer:=0;
		variable i: integer :=0;
	begin
		if(rising_edge(clk)) then
			if(pwm_acc > conv_integer(s)) then
				pwm_acc := 0;
			end if;
 			if(pwm_acc < conv_integer(s)) then 
				pwm_acc := pwm_acc + 1;
				if(pwm_acc = conv_integer(s)) then
					speaker <= s(i);
					i := i + 1;
					pwm_acc := 0;
					
				end if;
			end if;
			if(i = 15) then
				i := 0;
			end if;
		end if;
			
	end process;
				

end Behavioral;