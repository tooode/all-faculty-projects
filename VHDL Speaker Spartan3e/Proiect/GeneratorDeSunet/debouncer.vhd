----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:34:43 12/13/2019 
-- Design Name: 
-- Module Name:    debouncer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
	Port( clk : in std_logic;
			d_in : in std_logic;
			q_out : out std_logic);
end debouncer;

architecture Behavioral of debouncer is
signal Q1, Q2, Q3 : std_logic;
begin

process(clk)
begin
   if (clk'event and clk = '1') then
         Q1 <= D_IN;
         Q2 <= Q1;
         Q3 <= Q2;
   end if;
end process;
 
q_out <= Q1 and Q2 and (not Q3);

end Behavioral;

