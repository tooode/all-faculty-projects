--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   01:56:03 01/13/2020
-- Design Name:   
-- Module Name:   D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/Proiect/GeneratorDeSunet/simulare.vhd
-- Project Name:  GeneratorDeSunet
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: GeneratorDeSunet
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY simulare IS
END simulare;
 
ARCHITECTURE behavior OF simulare IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT GeneratorDeSunet
    PORT(
         clk : IN  std_logic;
         sw1 : IN  std_logic;
         sw2 : IN  std_logic;
         sw3 : IN  std_logic;
         sw4 : IN  std_logic;
         SF_D : OUT  std_logic_vector(3 downto 0);
         SF_CE0 : OUT  std_logic;
         LCD_E : OUT  std_logic;
         LCD_RS : OUT  std_logic;
         LCD_RW : OUT  std_logic;
         speaker : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal sw1 : std_logic := '0';
   signal sw2 : std_logic := '0';
   signal sw3 : std_logic := '0';
   signal sw4 : std_logic := '0';

 	--Outputs
   signal SF_D : std_logic_vector(3 downto 0);
   signal SF_CE0 : std_logic;
   signal LCD_E : std_logic;
   signal LCD_RS : std_logic;
   signal LCD_RW : std_logic;
   signal speaker : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: GeneratorDeSunet PORT MAP (
          clk => clk,
          sw1 => sw1,
          sw2 => sw2,
          sw3 => sw3,
          sw4 => sw4,
          SF_D => SF_D,
          SF_CE0 => SF_CE0,
          LCD_E => LCD_E,
          LCD_RS => LCD_RS,
          LCD_RW => LCD_RW,
          speaker => speaker
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 
		sw1<='1';
		wait for 100 ns;
		sw1<='0';
		sw2<='1';
		wait for 100ns;
		sw2<='0';
		sw3<='1';
		wait for 100 ns;
		sw3<='0';
		sw4<='1';
		wait for 100ns;
		sw4<='0';
      wait;
   end process;

END;
