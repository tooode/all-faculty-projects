/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/Proiect/GeneratorDeSunet/PWM.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
int ieee_p_3620187407_sub_514432868_3965413181(char *, char *, char *);


static void work_a_2525893486_3212880686_p_0(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    int t5;
    char *t6;
    int t7;
    unsigned char t8;
    char *t9;
    char *t10;
    int t11;
    int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;

LAB0:    xsi_set_current_line(19, ng0);
    t1 = (t0 + 992U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3072);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(20, ng0);
    t3 = (t0 + 1648U);
    t4 = *((char **)t3);
    t5 = *((int *)t4);
    t3 = (t0 + 1192U);
    t6 = *((char **)t3);
    t3 = (t0 + 5288U);
    t7 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t6, t3);
    t8 = (t5 > t7);
    if (t8 != 0)
        goto LAB5;

LAB7:
LAB6:    xsi_set_current_line(23, ng0);
    t1 = (t0 + 1648U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = (t0 + 1192U);
    t4 = *((char **)t1);
    t1 = (t0 + 5288U);
    t7 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t4, t1);
    t2 = (t5 < t7);
    if (t2 != 0)
        goto LAB8;

LAB10:
LAB9:    xsi_set_current_line(32, ng0);
    t1 = (t0 + 1768U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t2 = (t5 == 15);
    if (t2 != 0)
        goto LAB14;

LAB16:
LAB15:    goto LAB3;

LAB5:    xsi_set_current_line(21, ng0);
    t9 = (t0 + 1648U);
    t10 = *((char **)t9);
    t9 = (t10 + 0);
    *((int *)t9) = 0;
    goto LAB6;

LAB8:    xsi_set_current_line(24, ng0);
    t6 = (t0 + 1648U);
    t9 = *((char **)t6);
    t11 = *((int *)t9);
    t12 = (t11 + 1);
    t6 = (t0 + 1648U);
    t10 = *((char **)t6);
    t6 = (t10 + 0);
    *((int *)t6) = t12;
    xsi_set_current_line(25, ng0);
    t1 = (t0 + 1648U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = (t0 + 1192U);
    t4 = *((char **)t1);
    t1 = (t0 + 5288U);
    t7 = ieee_p_3620187407_sub_514432868_3965413181(IEEE_P_3620187407, t4, t1);
    t2 = (t5 == t7);
    if (t2 != 0)
        goto LAB11;

LAB13:
LAB12:    goto LAB9;

LAB11:    xsi_set_current_line(26, ng0);
    t6 = (t0 + 1192U);
    t9 = *((char **)t6);
    t6 = (t0 + 1768U);
    t10 = *((char **)t6);
    t11 = *((int *)t10);
    t12 = (t11 - 15);
    t13 = (t12 * -1);
    xsi_vhdl_check_range_of_index(15, 0, -1, t11);
    t14 = (1U * t13);
    t15 = (0 + t14);
    t6 = (t9 + t15);
    t8 = *((unsigned char *)t6);
    t16 = (t0 + 3152);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    *((unsigned char *)t20) = t8;
    xsi_driver_first_trans_fast_port(t16);
    xsi_set_current_line(27, ng0);
    t1 = (t0 + 1768U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t7 = (t5 + 1);
    t1 = (t0 + 1768U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t7;
    xsi_set_current_line(28, ng0);
    t1 = (t0 + 1648U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 0;
    goto LAB12;

LAB14:    xsi_set_current_line(33, ng0);
    t1 = (t0 + 1768U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = 0;
    goto LAB15;

}


extern void work_a_2525893486_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2525893486_3212880686_p_0};
	xsi_register_didat("work_a_2525893486_3212880686", "isim/simulare_isim_beh.exe.sim/work/a_2525893486_3212880686.didat");
	xsi_register_executes(pe);
}
