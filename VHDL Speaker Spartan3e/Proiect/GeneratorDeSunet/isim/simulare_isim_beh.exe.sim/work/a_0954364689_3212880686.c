/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/Proiect/GeneratorDeSunet/GeneratorDeSunet.vhd";



static void work_a_0954364689_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    xsi_set_current_line(61, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB5;

LAB6:    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB7;

LAB8:    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB9;

LAB10:    xsi_set_current_line(74, ng0);
    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t6 = (0 - 0);
    t7 = (t6 * 1);
    t8 = (16U * t7);
    t9 = (0 + t8);
    t1 = (t2 + t9);
    t5 = (t0 + 4832);
    t10 = (t5 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 16U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(75, ng0);
    t1 = (t0 + 9558);
    t5 = (t0 + 4896);
    t10 = (t5 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 64U);
    xsi_driver_first_trans_fast(t5);

LAB3:    t1 = (t0 + 4752);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(62, ng0);
    t1 = (t0 + 2792U);
    t5 = *((char **)t1);
    t6 = (1 - 0);
    t7 = (t6 * 1);
    t8 = (16U * t7);
    t9 = (0 + t8);
    t1 = (t5 + t9);
    t10 = (t0 + 4832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 16U);
    xsi_driver_first_trans_fast(t10);
    xsi_set_current_line(63, ng0);
    t1 = (t0 + 9302);
    t5 = (t0 + 4896);
    t10 = (t5 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 64U);
    xsi_driver_first_trans_fast(t5);
    goto LAB3;

LAB5:    xsi_set_current_line(65, ng0);
    t1 = (t0 + 2792U);
    t5 = *((char **)t1);
    t6 = (2 - 0);
    t7 = (t6 * 1);
    t8 = (16U * t7);
    t9 = (0 + t8);
    t1 = (t5 + t9);
    t10 = (t0 + 4832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 16U);
    xsi_driver_first_trans_fast(t10);
    xsi_set_current_line(66, ng0);
    t1 = (t0 + 9366);
    t5 = (t0 + 4896);
    t10 = (t5 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 64U);
    xsi_driver_first_trans_fast(t5);
    goto LAB3;

LAB7:    xsi_set_current_line(68, ng0);
    t1 = (t0 + 2792U);
    t5 = *((char **)t1);
    t6 = (3 - 0);
    t7 = (t6 * 1);
    t8 = (16U * t7);
    t9 = (0 + t8);
    t1 = (t5 + t9);
    t10 = (t0 + 4832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 16U);
    xsi_driver_first_trans_fast(t10);
    xsi_set_current_line(69, ng0);
    t1 = (t0 + 9430);
    t5 = (t0 + 4896);
    t10 = (t5 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 64U);
    xsi_driver_first_trans_fast(t5);
    goto LAB3;

LAB9:    xsi_set_current_line(71, ng0);
    t1 = (t0 + 2792U);
    t5 = *((char **)t1);
    t6 = (4 - 0);
    t7 = (t6 * 1);
    t8 = (16U * t7);
    t9 = (0 + t8);
    t1 = (t5 + t9);
    t10 = (t0 + 4832);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 16U);
    xsi_driver_first_trans_fast(t10);
    xsi_set_current_line(72, ng0);
    t1 = (t0 + 9494);
    t5 = (t0 + 4896);
    t10 = (t5 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 64U);
    xsi_driver_first_trans_fast(t5);
    goto LAB3;

}


extern void work_a_0954364689_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0954364689_3212880686_p_0};
	xsi_register_didat("work_a_0954364689_3212880686", "isim/simulare_isim_beh.exe.sim/work/a_0954364689_3212880686.didat");
	xsi_register_executes(pe);
}
