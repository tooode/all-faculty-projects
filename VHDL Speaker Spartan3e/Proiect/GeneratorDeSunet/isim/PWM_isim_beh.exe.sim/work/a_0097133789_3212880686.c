/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/Facultate/Proiect/An3Semestrul1/StructuraSistemelordeCalcul/Proiect/GeneratorDeSunet/PWM.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3499444699;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
char *ieee_p_3499444699_sub_2213602152_3536714472(char *, char *, int , int );


static void work_a_0097133789_3212880686_p_0(char *t0)
{
    char t11[16];
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    int t5;
    unsigned char t6;
    char *t7;
    int t8;
    int t9;
    char *t10;
    unsigned int t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 992U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 2912);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(44, ng0);
    t3 = (t0 + 1488U);
    t4 = *((char **)t3);
    t5 = *((int *)t4);
    t6 = (t5 < 5000);
    if (t6 != 0)
        goto LAB5;

LAB7:    t1 = (t0 + 1488U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t2 = (t5 == 5000);
    if (t2 != 0)
        goto LAB8;

LAB9:
LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(45, ng0);
    t3 = (t0 + 1488U);
    t7 = *((char **)t3);
    t8 = *((int *)t7);
    t9 = (t8 + 1);
    t3 = (t0 + 1488U);
    t10 = *((char **)t3);
    t3 = (t10 + 0);
    *((int *)t3) = t9;
    goto LAB6;

LAB8:    xsi_set_current_line(47, ng0);
    t1 = (t0 + 1608U);
    t4 = *((char **)t1);
    t8 = *((int *)t4);
    t9 = (t8 + 1);
    t1 = (t0 + 1608U);
    t7 = *((char **)t1);
    t1 = (t7 + 0);
    *((int *)t1) = t9;
    xsi_set_current_line(48, ng0);
    t1 = (t0 + 1488U);
    t3 = *((char **)t1);
    t1 = (t3 + 0);
    *((int *)t1) = 0;
    xsi_set_current_line(49, ng0);
    t1 = (t0 + 1608U);
    t3 = *((char **)t1);
    t5 = *((int *)t3);
    t1 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t11, t5, 4);
    t4 = (t11 + 12U);
    t12 = *((unsigned int *)t4);
    t12 = (t12 * 1U);
    t2 = (4U != t12);
    if (t2 == 1)
        goto LAB10;

LAB11:    t7 = (t0 + 2992);
    t10 = (t7 + 56U);
    t13 = *((char **)t10);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t1, 4U);
    xsi_driver_first_trans_fast_port(t7);
    goto LAB6;

LAB10:    xsi_size_not_matching(4U, t12, 0);
    goto LAB11;

}


extern void work_a_0097133789_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0097133789_3212880686_p_0};
	xsi_register_didat("work_a_0097133789_3212880686", "isim/PWM_isim_beh.exe.sim/work/a_0097133789_3212880686.didat");
	xsi_register_executes(pe);
}
