library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity lab7_4 is
    Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (4 downto 0);
           sw : in STD_LOGIC_VECTOR (15 downto 0);
           led : out STD_LOGIC_VECTOR (15 downto 0);
           an : out STD_LOGIC_VECTOR (3 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0);
           rx : in STD_LOGIC;
           tx : out STD_LOGIC);
end lab7_4;

architecture Behavioral of lab7_4 is

component MPG is
    Port ( en : out STD_LOGIC;
           input : in STD_LOGIC;
           clock : in STD_LOGIC);
end component;

component SSD is
    Port ( clk: in STD_LOGIC;
           digits: in STD_LOGIC_VECTOR(15 downto 0);
           an: out STD_LOGIC_VECTOR(3 downto 0);
           cat: out STD_LOGIC_VECTOR(6 downto 0));
end component;

component IFetch
    Port ( clk: in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           BranchAddress : in STD_LOGIC_VECTOR(15 downto 0);
           JumpAddress : in STD_LOGIC_VECTOR(15 downto 0);
           Jump : in STD_LOGIC;
           PCSrc : in STD_LOGIC;
           Instruction : out STD_LOGIC_VECTOR(15 downto 0);
           PCinc : out STD_LOGIC_VECTOR(15 downto 0));
end component;

component IDecode
    Port ( clk: in STD_LOGIC;
           en : in STD_LOGIC;    
           Instr : in STD_LOGIC_VECTOR(12 downto 0);
           WD : in STD_LOGIC_VECTOR(15 downto 0);
           RegWrite : in STD_LOGIC;
           RegDst : in STD_LOGIC;
           ExtOp : in STD_LOGIC;
           RD1 : out STD_LOGIC_VECTOR(15 downto 0);
           RD2 : out STD_LOGIC_VECTOR(15 downto 0);
           Ext_Imm : out STD_LOGIC_VECTOR(15 downto 0);
           func : out STD_LOGIC_VECTOR(2 downto 0);
           sa : out STD_LOGIC);
end component;

component MainControl
    Port ( Instr : in STD_LOGIC_VECTOR(2 downto 0);
           RegDst : out STD_LOGIC;
           ExtOp : out STD_LOGIC;
           ALUSrc : out STD_LOGIC;
           Branch : out STD_LOGIC;
           Jump : out STD_LOGIC;
           ALUOp : out STD_LOGIC_VECTOR(2 downto 0);
           MemWrite : out STD_LOGIC;
           MemtoReg : out STD_LOGIC;
           RegWrite : out STD_LOGIC);
end component;

component ExecutionUnit is
    Port ( PCinc : in STD_LOGIC_VECTOR(15 downto 0);
           RD1 : in STD_LOGIC_VECTOR(15 downto 0);
           RD2 : in STD_LOGIC_VECTOR(15 downto 0);
           Ext_Imm : in STD_LOGIC_VECTOR(15 downto 0);
           func : in STD_LOGIC_VECTOR(2 downto 0);
           sa : in STD_LOGIC;
           ALUSrc : in STD_LOGIC;
           ALUOp : in STD_LOGIC_VECTOR(2 downto 0);
           BranchAddress : out STD_LOGIC_VECTOR(15 downto 0);
           ALURes : out STD_LOGIC_VECTOR(15 downto 0);
           Zero : out STD_LOGIC);
end component;

component MEM
    port ( clk : in STD_LOGIC;
           en : in STD_LOGIC;
           ALUResIn : in STD_LOGIC_VECTOR(15 downto 0);
           RD2 : in STD_LOGIC_VECTOR(15 downto 0);
           MemWrite : in STD_LOGIC;			
           MemData : out STD_LOGIC_VECTOR(15 downto 0);
           ALUResOut : out STD_LOGIC_VECTOR(15 downto 0));
end component;

component Tx_fsm is
    Port (
            CLK : in STD_LOGIC;
            RST : in STD_LOGIC;
            BAUD_EN : in STD_LOGIC;
            TX_DATA : in STD_LOGIC_VECTOR (7 downto 0);
            TX_EN : in STD_LOGIC;
            TX_RDY : out STD_LOGIC;
            TX : out STD_LOGIC
          );
end component;

component Rx_fsm is
    Port (
        clk: in STD_LOGIC;
        RX_RDY: out std_logic;
        RX_DATA: out std_logic_Vector(7 downto 0);
        BAUD_EN: IN STD_LOGIC;
        rst:in std_logic;
        rx:in std_logic
        );
end component;

signal buttons : std_logic_vector (4 downto 0);
--signal Instruction, PCinc, RD1, RD2, WD, Ext_imm : STD_LOGIC_VECTOR(15 downto 0); 
--signal JumpAddress, BranchAddress, ALURes, ALURes1, MemData : STD_LOGIC_VECTOR(15 downto 0);
--signal func : STD_LOGIC_VECTOR(2 downto 0);
--signal sa, zero : STD_LOGIC;
signal digits : STD_LOGIC_VECTOR(15 downto 0);
--signal en, rst, PCSrc : STD_LOGIC; 
---- main controls 
--signal RegDst, ExtOp, ALUSrc, Branch, Jump, MemWrite, MemtoReg, RegWrite : STD_LOGIC;
--signal ALUOp :  STD_LOGIC_VECTOR(2 downto 0);
----registrii PIPELINE
--signal IfIdreg: STD_LOGIC_VECTOR(31 downto 0);
--signal IdExreg: STD_LOGIC_VECTOR(81 downto 0);
--signal ExMemreg: STD_LOGIC_VECTOR(55 downto 0);
--signal MemWbreg: STD_LOGIC_VECTOR(36 downto 0);
--TX_FSM
signal baud_en: std_logic;
signal tx_en: std_logic;
signal baud_counter: std_logic_vector(13 downto 0) := (others => '0');
signal tx_rdy: std_logic;
signal tx_data: std_logic_vector(7 downto 0);
--RX_FSM
signal baud_en2: std_logic;
signal baud_counter2: std_logic_vector(9 downto 0) := (others => '0');
signal rx_data: std_logic_vector(7 downto 0);
signal rx_rdy: std_logic;

begin

    -- buttons: reset, enable
--    monopulse1: MPG port map(en, btn(0), clk);
--    monopulse2: MPG port map(rst, btn(1), clk);
    monopulse3: MPG port map(buttons(1), btn(0) , clk);
    
--    -- main units
--    inst_IF: IFetch port map(clk, rst, en, ExMemreg(51 downto 36), JumpAddress, Jump, PCSrc, Instruction, PCinc);
--    inst_ID: IDecode port map(clk, en, Instruction(12 downto 0), WD, RegWrite, RegDst, ExtOp, RD1, RD2, Ext_imm, func, sa);
--    process(en)
--    begin
--    if rising_edge(en) then
--    IfIdreg(31 downto 16)<=PCinc;--PC+1
--    IfIdreg(15 downto 0)<=Instruction;--Instr
--    end if;
--    end process;
--    inst_MC: MainControl port map(IfIdreg(15 downto 13), RegDst, ExtOp, ALUSrc, Branch, Jump, ALUOp, MemWrite, MemtoReg, RegWrite);
--    inst_EX: ExecutionUnit port map(IfIdreg(15 downto 0), IdExReg(56 downto 41), IdExReg(40 downto 25), IdExReg(24 downto 9), IdExReg(8 downto 6), sa, IdExReg(74), IdExReg(77 downto 75), BranchAddress, ALURes, Zero); 
--    process(en)
--    begin
--    if rising_edge(en) then
--        IdExReg(81)<=MemToReg;
--        IdExReg(80)<=RegWrite;
--        IdExReg(79)<=MemWrite;
--        IdExReg(78)<=Branch;
--        IdExReg(77 downto 75)<=ALUOp;
--        IdExReg(74)<=ALUSrc;
--        IdExReg(73)<=RegDst;
--        IdExReg(72 downto 57)<=IfIdReg(31 downto 16);
--        IdExReg(56 downto 41)<=RD1;
--        IdExReg(40 downto 25)<=RD2;
--        IdExReg(24 downto 9)<=Ext_imm;
--        IdExReg(8 downto 6)<=func;
--        IdExReg(5 downto 3)<=IfIdReg(9 downto 7);
--        IdExReg(2 downto 0)<=IfIdReg(6 downto 4);
--    end if;
--    end process;
--    inst_MEM: MEM port map(clk, en, ExMemreg(51 downto 36), RD2, ExMemreg(53), MemData, ALURes1);
--    process(en)
--    begin
--    if rising_edge(en) then
--    ExMemreg(55)<=IdExReg(81);
--    ExMemreg(54)<=IdExReg(80);
--    ExMemreg(53)<=IdExReg(79);
--    ExMemreg(52)<=IdExReg(78);
--    ExMemreg(51 downto 36)<=BranchAddress;
--    ExMemreg(35 downto 20)<=ALURes;
--    ExMemreg(19 downto 4)<=IdExreg(40 downto 25);
--    ExMemreg(3)<=Zero;
--        if IdExReg(73) = '0' then
--            ExMemreg(2 downto 0)<=IdExreg(5 downto 3);
--        else
--            ExMemreg(2 downto 0)<=IdExreg(2 downto 0);
--        end if;
--    end if;
--   end process;
    
--    -- WriteBack unit
--    with MemtoReg select
--        WD <= MemWbreg(15 downto 0) when '0',
--              MemWbreg(34 downto 19) when '1',
--              (others => '0') when others;

--    -- branch control
--    PCSrc <= ExMemreg(3) and ExMemreg(52);

--    -- jump address
--    JumpAddress <= PCinc(15 downto 13) & Instruction(12 downto 0);
--    process(en)
--    begin
--    if rising_edge(en) then
--    MemWbreg(36)<= ExMemreg(55);
--    MemWbreg(35)<= ExMemreg(54);
--    MemWbreg(34 downto 19)<=ExMemreg(35 downto 20);
--    MemWbreg(18 downto 16)<=ExMemreg(2 downto 0);
--    MemWbreg(15 downto 0)<=MemData;
--    end if;
--    end process;
--   -- SSD display MUX
--    with sw(7 downto 5) select
--        digits <=  Instruction when "000", 
--                   PCinc when "001",
--                   RD1 when "010",
--                   RD2 when "011",
--                   Ext_Imm when "100",
--                   ALURes when "101",
--                   MemData when "110",
--                   WD when "111",
--                   (others => '0') when others; 

  display : SSD port map (clk, digits, an, cat);
    
--    -- main controls on the leds
--    led(10 downto 0) <= ALUOp & RegDst & ExtOp & ALUSrc & Branch & Jump & MemWrite & MemtoReg & RegWrite;
-- LABORATOR 11

process(clk)--TX baud_counter
begin
    if(rising_edge(clk)) then
        if(baud_counter = "10100010110000") then
            baud_counter <= "00000000000000";
            baud_en <= '1';
        else
            baud_en <= '0';
            baud_counter <= baud_counter + 1;
        end if;
    end if;
end process;

process(clk, baud_en, buttons(1))--TX_Enalbe
begin
    if(rising_edge(clk)) then
        if(buttons(1) = '1') then
            tx_en <= '1';
        end if;
        
        if(baud_en = '1') then
            tx_en <= '0';
        end if;
    end if;
end process;

process(clk)
begin
    if(rising_edge(clk)) then
        if (baud_counter2 = "1010001011") then
            baud_counter2 <= "0000000000";
            baud_en2 <= '1';
        else
            baud_en2 <= '0';
            baud_counter2 <= baud_counter2 + 1;
        end if;
    end if;
end process;

txfsm: tx_fsm port map (
        CLK => clk,
        RST => '0',
        BAUD_EN => baud_en,
        TX_DATA => sw(7 downto 0),
        TX_EN => tx_en,
        TX_RDY => tx_rdy,
        TX => tx
);

rxfsm: rx_fsm port map (
        clk => clk,
        RX_RDY => rx_rdy,
        RX_DATA => rx_data,
        BAUD_EN => baud_en2,
        rst => '0',
        rx => rx
);    

led(15 downto 0) <= b"0000_0000" & rx_data;
digits <= b"0000_0000" & rx_data;
end Behavioral;