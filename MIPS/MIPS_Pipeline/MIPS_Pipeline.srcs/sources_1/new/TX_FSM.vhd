library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Tx_fsm is
    Port (
            CLK : in STD_LOGIC;
            RST : in STD_LOGIC;
            BAUD_EN : in STD_LOGIC;
            TX_DATA : in STD_LOGIC_VECTOR (7 downto 0);
            TX_EN : in STD_LOGIC;
            TX_RDY : out STD_LOGIC;
            TX : out STD_LOGIC
          );
end Tx_fsm;

architecture Behavioral of Tx_fsm is

signal bit_cnt : std_logic_vector (2 downto 0);
signal state : std_logic_vector (1 downto 0);

begin

process (clk, rst, tx_en)
begin
    if (rst = '1') then
        state <= "00";
    elsif (rising_edge(clk)) then
        if(baud_en = '1') then
            case (state) is
                when "00" => 
                            if (tx_en = '1') then
                                state <= "01";
                                bit_cnt <= "000";
                            else
                                state <= "00";
                            end if;
                when "01" => state <= "10";
                when "10" => 
                            if (bit_cnt < "111") then
                                state <= "10";
                                bit_cnt <= bit_cnt + 1;
                            else
                                state <= "11";
                            end if;
                when "11" => state <= "00";
            end case;
        end if;
    end if;
end process;

process (state)
begin
    case (state) is
        when "00" => 
                tx <= '1';
                tx_rdy <= '1';
        when "01" => 
                tx <= '0';
                tx_rdy <= '0';
        when "10" =>
                tx <= tx_data(conv_integer(bit_cnt));
                tx_rdy <= '0';
        when "11" =>
                tx <= '1';
                tx_rdy <= '0';
    end case;
end process;

end Behavioral;
