library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity Rx_fsm is
    Port (
        clk: in STD_LOGIC;
        RX_RDY: out std_logic;
        RX_DATA: out std_logic_Vector(7 downto 0);
        BAUD_EN: IN STD_LOGIC;
        rst:in std_logic;
        rx:in std_logic
        );
end Rx_fsm;

architecture Behavioral of Rx_fsm is

signal baud_cnt: std_logic_vector(3 downto 0);
signal bit_cnt: std_logic_vector(2 downto 0);

type state_type is (idle, start, bits, stop, waits);
signal state: state_type;

begin
process(clk, rst, rx)
begin
    if (rst = '1') then
        state <= idle;
    elsif (rising_edge(clk)) then
        if (baud_en = '1') then
            case state is
                when idle =>
                    baud_cnt <= "0000";
                    bit_cnt <= "000";
                    if (rx = '0') then
                        state <= start;
                    end if;
                
                when start =>
                    if (rx = '1') then
                        state <= idle;
                    elsif (rx = '0' and baud_cnt = "0111") then
                        state <= bits;
                    elsif (baud_cnt < "0111") then
                        baud_cnt <= baud_cnt + 1;
                        state <= start;
                    end if;
                    
                when bits =>
                    baud_cnt <= baud_cnt + 1;
                    if(bit_cnt < "111" and baud_cnt = "1111") then
                        baud_cnt <= baud_cnt + 1;
                        rx_data(conv_integer(bit_cnt)) <= rx;
                        bit_cnt <= bit_cnt + 1;
                        baud_cnt <= "0000";
                        state <= bits;
                    elsif (bit_cnt = "111" and baud_cnt = "1111") then
                        state <= stop;
                        baud_cnt <= "0000";
                    end if;
                
                when stop =>
                    if (baud_cnt < "1111") then
                        baud_cnt <= baud_cnt + 1;
                        state <= stop;
                    elsif (baud_cnt = "1111") then
                        state <= waits;
                        baud_cnt <= "0000";
                    end if;
                    
                when waits =>
                    if (baud_cnt < "0111") then
                        baud_cnt <= baud_cnt + 1;
                        state <= waits;
                    elsif (baud_cnt = "0111") then
                        state <= idle;
                    end if;
                
                when others => state <= idle;
            end case;
        end if;
    end if;
end process;

process(state)   
begin 
    case state is
        when idle => RX_RDY <='0';
        when start => RX_RDY <='0';
        when bits => RX_RDY <='0';
        when stop => RX_RDY <='0';
        when waits => RX_RDY <='1';
    end case;  
end process; 

end Behavioral;
