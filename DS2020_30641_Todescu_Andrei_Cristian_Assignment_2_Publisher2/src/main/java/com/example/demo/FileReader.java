package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@RestController
@CrossOrigin
@RequestMapping("/patientactivity")
public class FileReader {

    private final PatientActivityPublisher activity;

    @Autowired
    public FileReader(PatientActivityPublisher activity) {
        this.activity = activity;
    }

    public List<PatientActivityDTO> read() {
        ArrayList<PatientActivityDTO> activities = new ArrayList<>();
        String filename = "activity.txt";

        try(Stream<String> stream = Files.lines(Paths.get(filename))){
            stream.forEach( line -> {
                try {
                    String[] splited = line.split("\\t\\t");
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date data1 = formatter.parse(splited[0]);
                    Date data2 = formatter.parse(splited[1]);
                    PatientActivityDTO newActivity = new PatientActivityDTO(UUID.fromString("7e1466a8-666d-48b2-a6db-6c980256f936"),data1,data2, splited[2]);
                    activities.add(newActivity);
                    System.out.println(newActivity);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } );
        }catch (IOException e){
            e.printStackTrace();
        }
        return activities;
    }

    @GetMapping("/addAct")
    public void addActivities(){
        List<PatientActivityDTO> activities = this.read();
        for(PatientActivityDTO activ: activities)
            activity.addActivity(activ);
    }
}
