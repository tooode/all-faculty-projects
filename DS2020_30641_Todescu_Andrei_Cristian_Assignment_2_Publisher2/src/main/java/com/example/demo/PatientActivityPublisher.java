package com.example.demo;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/patientactivity")
public class PatientActivityPublisher {

    @Autowired
    private RabbitTemplate template;

    @PostMapping("/{addActivity}")
    public String addActivity(@RequestBody PatientActivityDTO activity){
        System.out.println(activity);
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, activity);
        return "Activity succesfully added!";
    }




}
